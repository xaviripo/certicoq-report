#include "prim_floats.h"
#include "prim_int63.h"
#include "coq_c_ffi.h"

struct stack_frame;
struct thread_info;
struct stack_frame {
  unsigned long long *next;
  unsigned long long *root;
  struct stack_frame *prev;
};

struct thread_info {
  unsigned long long *alloc;
  unsigned long long *limit;
  struct heap *heap;
  unsigned long long args[1024];
  struct stack_frame *fp;
  unsigned long long nalloc;
};

extern struct thread_info *make_tinfo(void);
extern unsigned long long *export(struct thread_info *);
extern void SHA256dshadsha_wrapper_102(struct thread_info *, unsigned long long, unsigned long long);
extern void SHA256dshadsha_known_101(struct thread_info *);
extern unsigned long long body(struct thread_info *);
extern void garbage_collect(struct thread_info *);
extern _Bool is_ptr(unsigned long long);
void SHA256dshadsha_wrapper_102(struct thread_info *, unsigned long long, unsigned long long);
void SHA256dshadsha_known_101(struct thread_info *);
unsigned long long body(struct thread_info *);
unsigned long long const body_info_129[2] = { 3LL, 0LL, };

unsigned long long const SHA256dshadsha_known_info_128[2] = { 24LL, 0LL, };

unsigned long long const SHA256dshadsha_wrapper_info_127[4] = { 0LL, 2LL,
  0LL, 1LL, };

void SHA256dshadsha_wrapper_102(struct thread_info *$tinfo, unsigned long long $env_122, unsigned long long $inp_123)
{
  struct stack_frame frame;
  unsigned long long root[0];
  register unsigned long long *$alloc;
  register unsigned long long *$limit;
  register unsigned long long *$args;
  register _Bool $arg;
  $alloc = (*$tinfo).alloc;
  $limit = (*$tinfo).limit;
  $args = (*$tinfo).args;
  frame.next = root;
  frame.root = root;
  frame.prev = (*$tinfo).fp;
  $args = (*$tinfo).args;
  (*$tinfo).alloc = $alloc;
  (*$tinfo).limit = $limit;
  ((void (*)(struct thread_info *)) SHA256dshadsha_known_101)($tinfo);
}

void SHA256dshadsha_known_101(struct thread_info *$tinfo)
{
  struct stack_frame frame;
  unsigned long long root[0];
  register unsigned long long $y_105;
  register unsigned long long $y_106;
  register unsigned long long $y_107;
  register unsigned long long $y_108;
  register unsigned long long $y_109;
  register unsigned long long $y_110;
  register unsigned long long $y_111;
  register unsigned long long $y_112;
  register unsigned long long $y_113;
  register unsigned long long $y_114;
  register unsigned long long $y_115;
  register unsigned long long $y_116;
  register unsigned long long $y_117;
  register unsigned long long $y_118;
  register unsigned long long $y_119;
  register unsigned long long $y_120;
  register unsigned long long $y_121;
  register unsigned long long *$alloc;
  register unsigned long long *$limit;
  register unsigned long long *$args;
  register _Bool $arg;
  $alloc = (*$tinfo).alloc;
  $limit = (*$tinfo).limit;
  $args = (*$tinfo).args;
  frame.next = root;
  frame.root = root;
  frame.prev = (*$tinfo).fp;
  if (!(24LLU <= $limit - $alloc)) {
    /*skip*/;
    (*$tinfo).nalloc = 24LLU;
    garbage_collect($tinfo);
    /*skip*/;
    $alloc = (*$tinfo).alloc;
    $limit = (*$tinfo).limit;
  }
  $y_105 = 1LLU;
  $y_106 = 3LLU;
  $y_107 = 5LLU;
  $y_108 = 7LLU;
  $y_109 = 9LLU;
  $y_110 = 11LLU;
  $y_111 = 13LLU;
  $y_112 = 15LLU;
  $y_113 = 1LLU;
  $y_114 = (unsigned long long) ($alloc + 1LLU);
  $alloc = $alloc + 3LLU;
  *((unsigned long long *) $y_114 + 18446744073709551615LLU) = 2048LLU;
  *((unsigned long long *) $y_114 + 0LLU) = $y_112;
  *((unsigned long long *) $y_114 + 1LLU) = $y_113;
  $y_115 = (unsigned long long) ($alloc + 1LLU);
  $alloc = $alloc + 3LLU;
  *((unsigned long long *) $y_115 + 18446744073709551615LLU) = 2048LLU;
  *((unsigned long long *) $y_115 + 0LLU) = $y_111;
  *((unsigned long long *) $y_115 + 1LLU) = $y_114;
  $y_116 = (unsigned long long) ($alloc + 1LLU);
  $alloc = $alloc + 3LLU;
  *((unsigned long long *) $y_116 + 18446744073709551615LLU) = 2048LLU;
  *((unsigned long long *) $y_116 + 0LLU) = $y_110;
  *((unsigned long long *) $y_116 + 1LLU) = $y_115;
  $y_117 = (unsigned long long) ($alloc + 1LLU);
  $alloc = $alloc + 3LLU;
  *((unsigned long long *) $y_117 + 18446744073709551615LLU) = 2048LLU;
  *((unsigned long long *) $y_117 + 0LLU) = $y_109;
  *((unsigned long long *) $y_117 + 1LLU) = $y_116;
  $y_118 = (unsigned long long) ($alloc + 1LLU);
  $alloc = $alloc + 3LLU;
  *((unsigned long long *) $y_118 + 18446744073709551615LLU) = 2048LLU;
  *((unsigned long long *) $y_118 + 0LLU) = $y_108;
  *((unsigned long long *) $y_118 + 1LLU) = $y_117;
  $y_119 = (unsigned long long) ($alloc + 1LLU);
  $alloc = $alloc + 3LLU;
  *((unsigned long long *) $y_119 + 18446744073709551615LLU) = 2048LLU;
  *((unsigned long long *) $y_119 + 0LLU) = $y_107;
  *((unsigned long long *) $y_119 + 1LLU) = $y_118;
  $y_120 = (unsigned long long) ($alloc + 1LLU);
  $alloc = $alloc + 3LLU;
  *((unsigned long long *) $y_120 + 18446744073709551615LLU) = 2048LLU;
  *((unsigned long long *) $y_120 + 0LLU) = $y_106;
  *((unsigned long long *) $y_120 + 1LLU) = $y_119;
  $y_121 = (unsigned long long) ($alloc + 1LLU);
  $alloc = $alloc + 3LLU;
  *((unsigned long long *) $y_121 + 18446744073709551615LLU) = 2048LLU;
  *((unsigned long long *) $y_121 + 0LLU) = $y_105;
  *((unsigned long long *) $y_121 + 1LLU) = $y_120;
  *($args + 1LLU) = $y_121;
  (*$tinfo).alloc = $alloc;
  (*$tinfo).limit = $limit;
}

unsigned long long body(struct thread_info *$tinfo)
{
  struct stack_frame frame;
  unsigned long long root[0];
  register unsigned long long $env_125;
  register unsigned long long $SHA256dshadsha_wrapper_clo_126;
  register unsigned long long *$alloc;
  register unsigned long long *$limit;
  register unsigned long long *$args;
  $alloc = (*$tinfo).alloc;
  $limit = (*$tinfo).limit;
  $args = (*$tinfo).args;
  frame.next = root;
  frame.root = root;
  frame.prev = (*$tinfo).fp;
  if (!(3LLU <= $limit - $alloc)) {
    /*skip*/;
    (*$tinfo).nalloc = 3LLU;
    garbage_collect($tinfo);
    /*skip*/;
    $alloc = (*$tinfo).alloc;
    $limit = (*$tinfo).limit;
  }
  $env_125 = 1LLU;
  $SHA256dshadsha_wrapper_clo_126 = (unsigned long long) ($alloc + 1LLU);
  $alloc = $alloc + 3LLU;
  *((unsigned long long *) $SHA256dshadsha_wrapper_clo_126
     + 18446744073709551615LLU) =
    2048LLU;
  *((unsigned long long *) $SHA256dshadsha_wrapper_clo_126 + 0LLU) =
    SHA256dshadsha_wrapper_102;
  *((unsigned long long *) $SHA256dshadsha_wrapper_clo_126 + 1LLU) =
    $env_125;
  *($args + 1LLU) = $SHA256dshadsha_wrapper_clo_126;
  (*$tinfo).alloc = $alloc;
  (*$tinfo).limit = $limit;
  return *((unsigned long long *) (*$tinfo).args + 1LLU);
}


