// For file reading purposes:
#include <stdio.h>
#include <stdlib.h>

#include "gc_stack.h"
#include "prim_int63.h"

extern void* call(struct thread_info *tinfo, unsigned long long clos, unsigned long long arg0);
extern value make_Coq_Numbers_BinNums_positive_xH(void);
extern value make_Coq_Init_Datatypes_list_nil(void);
extern value alloc_make_Coq_Init_Datatypes_list_cons(struct thread_info *tinfo, unsigned long long, unsigned long long);
extern void print_Coq_Init_Datatypes_list(value, void (*)(value));
extern value get_Coq_Init_Datatypes_cons_args(value);
extern value get_Coq_Init_Datatypes_list_tag(value);

extern primint prim_int63_add(primint x, primint y);
extern primint prim_int63_mul(primint x, primint y);

_Bool is_ptr(value s) {
  return (_Bool) Is_block(s);
}

/**
 * Translate a (C) array of char to a (Coq) list of uint
 * @param tinfo the global thread_info
 * @param arr_size the size of the array to convert
 * @param arr a pointer to the array to convert
 * @param l a pointer to the empty list to populate
*/
void array_char_to_list_uint(struct thread_info *tinfo, unsigned int arr_size, char *arr, value *l) {
  for (int i = arr_size - 1; i >= 0; i--) {
    value nu = Val_long((unsigned int) arr[i]);
    *l = alloc_make_Coq_Init_Datatypes_list_cons(tinfo, nu, *l);
  }
}

/**
 * Translate a (Coq) list of uint to a (C) array of unsigned int
 * @param size the size of the list
 * @param l a pointer to the list to convert
 * @param arr a pointer to the empty array to populate
*/
void list_uint_to_array_unsigned_int(int size, value *l, unsigned int *arr) {
  value temp = *l;
  int i = 0;
  while (1) {
    unsigned int tag = get_Coq_Init_Datatypes_list_tag(temp);
    if (tag == 0) {
      break;
    } else if (tag == 1) {
      value *args = get_Coq_Init_Datatypes_cons_args(temp);
      arr[i] = Unsigned_long_val(args[0]);
      temp = args[1];
      i++;
    }
  }
}

/**
 * Prints an array arr of size arr_size
 * @param arr_size the size of the array to print
 * @param arr a pointer to the array to print
*/
void print_array(int arr_size, unsigned int *arr) {
  for (int i = 0; i < arr_size; i++) {
    printf("%u ", arr[i]);
  }
  printf("\n");
}

/**
 * This program takes a filename, opens and reads the corresponding file, and applies the SHA
 * algorithm to the contents of the file. Finally, it prints to stdout the result of the SHA
 * algorithm, as 8 unsigned integers separated by a space.
 * In particular, this is a modified version of SHA that always returns 0 1 2 3 4 5 6 7
 * It takes a single argument, argv[1], a string containing the filename of the file to process.
*/
int main(int argc, char **argv) {

  // Initial machinery and obtaining the SHA function
  struct thread_info* tinfo = make_tinfo();
  body(tinfo);
  value sha = tinfo->args[1];

  // Read the file using the first argument to the program as the filename
  FILE *f = fopen(argv[1], "rb");

  // Calculate length of file and rewind pointer to actually read the data
  fseek(f, 0, SEEK_END);
  long fsize = ftell(f);
  fseek(f, 0, SEEK_SET); // same as rewind(f);

  // Read data into string and set terminator character
  char *string = malloc(fsize + 1);
  fread(string, fsize, 1, f);
  fclose(f);
  string[fsize] = 0;

  // Create and populate list with string data
  value *list = make_Coq_Init_Datatypes_list_nil();
  array_char_to_list_uint(tinfo, fsize, string, &list);
  list = call(tinfo, sha, list);

  // Declare the output array and its size
  int arr_size = 8;
  unsigned int *arr = (unsigned int *) malloc(arr_size * sizeof(unsigned int));

  // Make sure the array is all set to 0, just in case
  for (int i = 0; i < arr_size; i++) {
    arr[i] = 0;
  }

  // Write the values from the list into the array
  list_uint_to_array_unsigned_int(arr_size, &list, arr);

  // Print the array
  print_array(arr_size, arr);

  // Free the pointers
  free(arr);
  free(string);

}
