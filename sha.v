Set Warnings "-notation-overridden, -ambiguous-paths".
From mathcomp Require Import all_ssreflect.
From formalv Require Import all_prim63_mathcomp.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope uint63_scope.

Definition sha (inp : seq uint) := (* Here would go all the logic of SHA *) [:: 0x0; 0x1; 0x2; 0x3; 0x4; 0x5; 0x6; 0x7].
