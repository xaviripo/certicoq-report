# Instructions

```sh
coq_makefile -f _CoqProject -o Makefile && make
cd c
coq_makefile -f _CoqProject -o Makefile && make
# Change the includes in sha.c to:
# #include "prim_floats.h"
# #include "prim_int63.h"
# #include "coq_c_ffi.h"
gcc -Wno-everything -O2 -fomit-frame-pointer -I values.h gc_stack.c sha.c glue.sha.c main.c prim_int63.c
```
